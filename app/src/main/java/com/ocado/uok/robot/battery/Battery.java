package com.ocado.uok.robot.battery;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class Battery extends ViewModel {

    private Handler handler = new Handler(Looper.getMainLooper());

    private final static Integer MAX_BATTERY_LEVEL = 30;
    private final MutableLiveData<Integer> power = new MutableLiveData<>();
    private Integer currentBatteryLevel = MAX_BATTERY_LEVEL;

    public Battery() {
        recharge();
        consumeBattery();
    }

    public void recharge() {
        currentBatteryLevel = MAX_BATTERY_LEVEL;
        power.postValue(100);
        Log.d("Battery", "Battery fully charged.");
    }

    private void consumeBattery() {
        handler.postDelayed(() -> {
            currentBatteryLevel = positiveSubtract(currentBatteryLevel, 1);
            power.postValue(currentBatteryLevel * 100 / MAX_BATTERY_LEVEL);
            consumeBattery();
        }, 1000);
        Log.d("Battery", String.format("power: %d%%", power.getValue()));
    }

    private Integer positiveSubtract(int a, int b) {
        return Math.max(0, a - b);
    }

    public MutableLiveData<Integer> getPower() {
        return power;
    }
}
