package com.ocado.uok.robot;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;

public class EmptyBatteryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emptybattery);
    }
}
