package com.ocado.uok.robot;

import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;

import com.ocado.uok.robot.basket.Product;
import com.ocado.uok.robot.basket.ShoppingTask;
import com.ocado.uok.robot.battery.Battery;
import com.ocado.uok.robot.result.ShoppingTaskResults;
import com.ocado.uok.robot.time.TimeRecord;
import com.ocado.uok.skilo.scanner.ScannerResult;

import static com.ocado.uok.robot.R.layout;

/**
 * Ekran ze skanowaniem produktów.
 */
public class ScanActivity extends ActivityWithSomeMethods {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(layout.activity_scan);

        ScannerResult scannerResult =
                ViewModelProviders.of(this).get(ScannerResult.class);
        ShoppingTask shoppingTask =
                ViewModelProviders.of(this).get(ShoppingTask.class);
        ShoppingTaskResults shoppingTaskResults =
                ViewModelProviders.of(this).get(ShoppingTaskResults.class);
        Battery battery = handleBatteryConsumption();

        shoppingTask.setProductsToBuy(Product.buildRandomList());
        getTimer().start();

        scannerResult.getLastScanned().observe(this, scannedText -> {
            if (scannedText.equals("POWER")) {
                battery.recharge();
            } else {
                int ileDoKupienia = 0;
                Product product = Product.of(scannedText);
                for (Product p : shoppingTask.getProductsToBuy()) {
                    if (p == product) {
                        ileDoKupienia++;
                    }
                }

                int ileWKoszyku = 0;
                for (Product p : shoppingTask.getProductsInBasket()) {
                    if (p == product) {
                        ileWKoszyku++;
                    }
                }
                if (ileWKoszyku < ileDoKupienia) {
                    shoppingTask.addToBasket(product);
                    if (shoppingTask.isComplete()) {
                        TimeRecord timeRecord = getTimer().end();
                        shoppingTaskResults.save(timeRecord);
                        getAudioPlayer().play(R.raw.success);
                        displayDialogWithAction(
                                "GRATULACJE!",
                                "Całe zamówienie skompletowane w czasie " + timeRecord.getReadableDuration(),
                                () -> finish());
                    } else {
                        getAudioPlayer().play(R.raw.purchase);
                        showToast("Dodano do koszyka " + scannedText);
                    }
                } else {
                    getAudioPlayer().play(R.raw.error);
                    showToast(scannedText + " nie jest do zebrania");
                }
            }

            doWithDelay(1000, () -> scannerResult.getResumeCamera().call());
        });
    }

    private Battery handleBatteryConsumption() {
        Battery battery = ViewModelProviders.of(this).get(Battery.class);
        battery.getPower().observe(this, batteryLevel -> {
                    if (batteryLevel == 0) {
                        finish();
                        this.startActivity(new Intent(this, EmptyBatteryActivity.class));
                    }
                }
        );
        return battery;
    }

}
