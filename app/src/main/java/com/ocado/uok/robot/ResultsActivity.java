package com.ocado.uok.robot;

import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ocado.uok.robot.result.ShoppingTaskResults;
import com.ocado.uok.robot.result.TimeRecordAdapter;
import com.ocado.uok.robot.time.TimeRecord;

import java.util.Collections;
import java.util.List;

public class ResultsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        ShoppingTaskResults shoppingTaskResults =
                ViewModelProviders.of(this).get(ShoppingTaskResults.class);

        shoppingTaskResults.getTimeRecords().observe(this, timeRecords -> {
            Collections.sort(timeRecords);
            presentRecords(timeRecords, R.id.resultView, R.layout.result_card, R.id.durationText, R.id.startTimeText, R.id.positionText);
        });
    }

    private void presentRecords(List<TimeRecord> records, int resultListViewId,
                                         int itemLayoutId, int durationTextId, int startTimeTextId, int positionTextId) {
        RecyclerView recyclerView = findViewById(resultListViewId);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new TimeRecordAdapter(records, itemLayoutId, durationTextId, startTimeTextId, positionTextId));
    }
}
